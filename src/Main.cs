using System;
using System.Diagnostics;
using System.Linq;
using Godot;

public class Main : Control
{
    // ReSharper disable UnassignedField.Global
    // ReSharper disable MemberCanBePrivate.Global
    [Export] public NodePath TurnCounterNodePath;
    [Export] public NodePath InputRegisterNodePath;
    [Export] public NodePath OutputRegisterNodePath;
    [Export] public NodePath OutputRegistersShiftNodePath;

    [Export] public NodePath[] InputRegisterSlidersNodePath;
    // ReSharper restore MemberCanBePrivate.Global
    // ReSharper restore UnassignedField.Global

    private LineEdit _turnCounterLineEdit;
    private LineEdit _inputRegisterLineEdit;
    private LineEdit _outputRegisterLineEdit;
    private Control _outputRegistersShiftControl;
    private Slider[] _inputRegisterSliders;

    private const int InputRegisterLength = 10;
    private const int OutputRegisterLength = 13;
    private const int OutputRegistersMaxOffset = 7;
    private const int TurnCounterLength = 8;
    private char[] _inputRegister = new char[InputRegisterLength];
    private char[] _outputRegister = new char[OutputRegisterLength];
    private char[] _turnCounter = new char[TurnCounterLength];
    private static char[] _oneRegister = { '1' };
#if false
    private byte _inputRegisterIndex;
#endif
    private sbyte _outputRegistersOffset;
    private bool _outputRegisterIsLocked;
    private bool _turnCounterIsComplemented;

    public override void _Ready()
    {
        InitializeNodes();
        Reset();
    }

    private void InitializeNodes()
    {
        _turnCounterLineEdit = GetNode<LineEdit>(TurnCounterNodePath);
        _inputRegisterLineEdit = GetNode<LineEdit>(InputRegisterNodePath);
        _outputRegisterLineEdit = GetNode<LineEdit>(OutputRegisterNodePath);
        _outputRegistersShiftControl = GetNode<Control>(OutputRegistersShiftNodePath);
        _InitializeSliders();
    }

    private void Reset()
    {
        ClearInputRegister();
        ClearOutputRegisters();
        SetOutputRegisterOffsetToMax();
        _outputRegisterIsLocked = false;
        _turnCounterIsComplemented = false;
    }

    private void SetOutputRegisterOffsetToMax()
    {
        _outputRegistersOffset = OutputRegistersMaxOffset;
    }

    private void SetOutputRegisterOffsetToMin()
    {
        _outputRegistersOffset = 0;
    }

    private void _InitializeSliders()
    {
        var length = InputRegisterSlidersNodePath.Length;
        _inputRegisterSliders = new Slider[length];
        for (var i = 0; i < length; i++)
        {
            _inputRegisterSliders[i] = GetNode<Slider>(InputRegisterSlidersNodePath[i]);
        }
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        RefreshInputRegister();
        RefreshOutputRegisters();
        UpdateOutputRegistersShift();
    }

    private void RefreshInputRegister()
    {
        _inputRegisterLineEdit.Text = String.Join(" ", _inputRegister);
    }

    private void RefreshOutputRegisters()
    {
        _outputRegisterLineEdit.Text = String.Join(" ", _outputRegister);
        var turnCounter = _turnCounterIsComplemented ? Complement10(ref _turnCounter) : _turnCounter;
        _turnCounterLineEdit.Text = String.Join(" ", turnCounter);
    }

    public static char[] Complement9(ref char[] input)
    {
        int length = input.Length;
        char[] output = new char[length];
        for (var i = 0; i < length; i++)
        {
            // var number = input[i] - '0';
            // var complement = 9 - number; /* number + complement = 9 => complement = 9 - number */
            // var output = complement + '0'; // = 9 - number + '0' = 9 - input[i] + '0' + '0'
            output[i] = (char)('9' - input[i] + '0');
        }

        return output;
    }

    public static char[] Complement10(ref char[] input)
    {
        char[] output = Complement9(ref input);
        AddArray(OperationType.Addition, ref output, 0, output.Length - 1, ref _oneRegister, 0);
        return output;
    }

    public override void _Input(InputEvent @event)
    {
#if false
        bool handled = false;
        if (@event is InputEventKey eventKey && eventKey.Pressed)
        {
            handled = true;
            switch (eventKey.Scancode)
            {
                case (uint) KeyList.Key0:
                case (uint) KeyList.Kp0:
                    InputNumber('0');
                    break;
                case (uint) KeyList.Key1:
                case (uint) KeyList.Kp1:
                    InputNumber('1');
                    break;
                case (uint) KeyList.Key2:
                case (uint) KeyList.Kp2:
                    InputNumber('2');
                    break;
                case (uint) KeyList.Key3:
                case (uint) KeyList.Kp3:
                    InputNumber('3');
                    break;
                case (uint) KeyList.Key4:
                case (uint) KeyList.Kp4:
                    InputNumber('4');
                    break;
                case (uint) KeyList.Key5:
                case (uint) KeyList.Kp5:
                    InputNumber('5');
                    break;
                case (uint) KeyList.Key6:
                case (uint) KeyList.Kp6:
                    InputNumber('6');
                    break;
                case (uint) KeyList.Key7:
                case (uint) KeyList.Kp7:
                    InputNumber('7');
                    break;
                case (uint) KeyList.Key8:
                case (uint) KeyList.Kp8:
                    InputNumber('8');
                    break;
                case (uint) KeyList.Key9:
                case (uint) KeyList.Kp9:
                    InputNumber('9');
                    break;
                case (uint) KeyList.Enter:
                case (uint) KeyList.KpEnter:
                case (uint) KeyList.Plus:
                case (uint) KeyList.KpAdd:
                case (uint) KeyList.Equal:
                    Execute(OperationType.Addition);
                    break;
                case (uint) KeyList.Minus:
                case (uint) KeyList.KpSubtract:
                    Execute(OperationType.Subtraction);
                    break;
                case (uint) KeyList.Escape:
                    ClearOutputRegisters();
                    break;
                case (uint) KeyList.Delete:
                case (uint) KeyList.Backspace:
                    ClearInputRegister();
                    break;
                case (uint) KeyList.Left:
                    ShiftLeft();
                    break;
                case (uint) KeyList.Right:
                    ShiftRight();
                    break;
                case (uint) KeyList.Capslock:
                    ToggleOutputRegisterLocked();
                    break;
                case (uint) KeyList.Tab:
                    ToggleTurnCounterComplemented();
                    break;
                case (uint) KeyList.Home:
                    SetOutputRegisterOffsetToMax();
                    break;
                case (uint) KeyList.End:
                    SetOutputRegisterOffsetToMin();
                    break;
                default:
                    handled = false;
                    break;
            }
        }

        if (handled)
        {
            GetTree().SetInputAsHandled();
        }
#endif
    }

    private void ToggleOutputRegisterLocked()
    {
        _outputRegisterIsLocked ^= true;
        var status = _outputRegisterIsLocked ? "locked" : "unlocked";
        GD.Print($"Output Register is {status}");
    }

    private void ToggleTurnCounterComplemented()
    {
        _turnCounterIsComplemented ^= true;
        var status = _turnCounterIsComplemented ? "complemented" : "normal";
        GD.Print($"Turn counter is {status}");
    }

    private void ClearInputRegister()
    {
        ClearArray(ref _inputRegister);
#if false
        _inputRegisterIndex = 0;
#endif

        InputRegisterToSliders();
    }

    private void InputRegisterToSliders()
    {
        for (var i = 0; i < InputRegisterLength; i++)
        {
            _inputRegisterSliders[i].Value = _inputRegister[i] - '0';
        }
    }

    private void ClearOutputRegisters()
    {
        if (!_outputRegisterIsLocked)
            ClearArray(ref _outputRegister);
        ClearArray(ref _turnCounter);
    }

    private static void ClearArray(ref char[] array, char value = '0')
    {
        for (var i = 0; i < array.Length; i++)
            array[i] = value;
    }


#if false
private void InputNumber(char digit)
    {

        if (!char.IsDigit(digit))
        {
            throw new Exception("Input argument must be a digit.");
        }

        if (_inputRegisterIndex < InputRegisterLength)
        {
            RotateInput(ref _inputRegister, digit);
            _inputRegisterIndex++;
        }

        InputRegisterToSliders();
    }
#endif

    public enum OperationType
    {
        Addition,
        Subtraction
    }

    private void Execute(OperationType type)
    {
        int outputRight = OutputRegisterLength - OutputRegistersMaxOffset + _outputRegistersOffset - 1;
        const int outputLeft = 0;
        const int inputRight = InputRegisterLength - 1;
        AddArray(type, ref _outputRegister, outputLeft, outputRight, ref _inputRegister, inputRight);

        int turnRight = TurnCounterLength - OutputRegistersMaxOffset + _outputRegistersOffset - 1;
        AddArray(type, ref _turnCounter, 0, turnRight, ref _oneRegister, 0);
    }

    private static void AddArray(OperationType type, ref char[] output, int outputLeft, int outputRight,
        ref char[] input, int inputRight)
    {
        int carryBorrow = 0;

        for (; outputRight >= outputLeft; outputRight--, inputRight--)
        {
            int result;
            var left = output[outputRight] - '0';
            var right = inputRight >= 0 ? input[inputRight] - '0' : 0;
            (carryBorrow, result) = Operation(type, left, right, carryBorrow);
            output[outputRight] = (char)(result + '0');
        }
    }

    public static (int, int) Operation(OperationType type, int left, int right, int carryBorrow)
    {
        int sign = type == OperationType.Addition ? 1 : -1;
        int result;

        result = left + sign * right + carryBorrow;
        carryBorrow = (int)Math.Floor(result / 10.0);
        result = ModuloEuclidean(result, 10);

        return (carryBorrow, result);
    }

#if false
    private void RotateInput(ref char[] register, char value)
    {
        int i;
        
        for (i = 0; i < register.Length - 1; i++)
        {
            register[i] = register[i + 1];
        }

        register[i] = value;
    }
#endif

    public static int ModuloEuclidean(int a, int b)
    {
        if (b == 0) throw new DivideByZeroException();
        if (b == -1) return 0; // This test needed to prevent UB of `INT_MIN % -1`.
        int m = a % b;
        if (m < 0)
        {
            // m += (b < 0) ? -b : b; // avoid this form: it is UB when b == INT_MIN
            m = (b < 0) ? m - b : m + b;
        }

        return m;
    }

    private void ShiftLeft()
    {
        _outputRegistersOffset++;
        if (_outputRegistersOffset > OutputRegistersMaxOffset)
        {
            _outputRegistersOffset = OutputRegistersMaxOffset;
        }

        GD.Print($"Register offset: {_outputRegistersOffset}/{OutputRegistersMaxOffset}");
    }

    private void ShiftRight()
    {
        _outputRegistersOffset--;
        if (_outputRegistersOffset < 0)
        {
            _outputRegistersOffset = 0;
        }

        GD.Print($"Register offset: {_outputRegistersOffset}/{OutputRegistersMaxOffset}");
    }

    private void UpdateOutputRegistersShift()
    {
        var tmp = _outputRegistersShiftControl.RectMinSize;
        tmp.x = 270 - (OutputRegistersMaxOffset - _outputRegistersOffset) * 32;
        _outputRegistersShiftControl.RectMinSize = tmp;
    }

    private void InputRegisterUpdateDigit(int index)
    {
        Debug.Assert(_inputRegisterSliders != null, nameof(_inputRegisterSliders) + " != null");
        var value = (char)((int)_inputRegisterSliders[index].Value + '0');
        _inputRegister[index] = value;
    }

    /* ---------- Public interface ------------------------------------------------------------------------ */
    public void EventAdd()
    {
        Execute(OperationType.Addition);
    }

    public void EventSubtract()
    {
        Execute(OperationType.Subtraction);
    }

    public void EventClearInput()
    {
        ClearInputRegister();
    }

    public void EventClearOutput()
    {
        ClearOutputRegisters();
    }

    public void EventToggleLockOutputRegister()
    {
        ToggleOutputRegisterLocked();
    }

    public void EventToggleComplementTurnCounter()
    {
        ToggleTurnCounterComplemented();
    }

    public void EventShiftLeft()
    {
        ShiftLeft();
    }

    public void EventShiftRight()
    {
        ShiftRight();
    }

    public void EventShiftLeftAll()
    {
        SetOutputRegisterOffsetToMax();
    }

    public void EventShiftRightAll()
    {
        SetOutputRegisterOffsetToMin();
    }

    public void InputRegisterValueChanged(double value, int index)
    {
        InputRegisterUpdateDigit(index);
        RefreshInputRegister();
    }
}