﻿using System;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    public class TestMainOperation
    {
        [Test]
        public void Test1()
        {
            int a = 1, b = 1, c = 0;
            (int carryOut, int result) = Main.Operation(Main.OperationType.Addition, a, b, c);

            Assert.AreEqual(0, carryOut, "Carry output");
            Assert.AreEqual(2, result, "Result");
        }

        [Test]
        public void Test2()
        {
            int a = 1, b = 1, c = 1;
            (int carryOut, int result) = Main.Operation(Main.OperationType.Addition, a, b, c);

            Assert.AreEqual(0, carryOut, "Carry output");
            Assert.AreEqual(3, result, "Result");
        }

        [Test]
        public void Test3()
        {
            int a = 8, b = 1, c = 1;
            (int carryOut, int result) = Main.Operation(Main.OperationType.Addition, a, b, c);

            Assert.AreEqual(1, carryOut, "Carry output");
            Assert.AreEqual(0, result, "Result");
        }

        [Test]
        public void Test4()
        {
            int a = 5, b = 1, c = 0;
            (int carryOut, int result) = Main.Operation(Main.OperationType.Subtraction, a, b, c);

            Assert.AreEqual(0, carryOut, "Carry output");
            Assert.AreEqual(4, result, "Result");
        }

        [Test]
        public void Test5()
        {
            int a = 0, b = 1, c = 0;
            (int carryOut, int result) = Main.Operation(Main.OperationType.Subtraction, a, b, c);

            Assert.AreEqual(-1, carryOut, "Carry output");
            Assert.AreEqual(9, result, "Result");
        }

        [Test]
        public void Test6()
        {
            int a = 0, b = 1, c = 0;
            int carryOut, result;
            (carryOut, result) = Main.Operation(Main.OperationType.Subtraction, a, b, c);

            Assert.AreEqual(-1, carryOut, "Carry output");
            Assert.AreEqual(9, result, "Result");
        }
    }

    [TestFixture]
    public class TestMainComplement9
    {
        [Test]
        public void Test1()
        {
            char[] input = "1234567890".ToCharArray();
            char[] result = Main.Complement9(ref input);

            string actual = String.Join("", result);
            string expected = "8765432109";
            
            Assert.AreEqual(expected, actual, "Complement");
        }
        
        [Test]
        public void Test2()
        {
            char[] input = "1234567890".ToCharArray();
            char[] result = Main.Complement10(ref input);

            string actual = String.Join("", result);
            string expected = "8765432110";
            
            Assert.AreEqual(expected, actual, "Complement");
        }
    }
}